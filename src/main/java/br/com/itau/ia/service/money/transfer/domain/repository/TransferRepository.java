package br.com.itau.ia.service.money.transfer.domain.repository;

import br.com.itau.ia.service.money.transfer.domain.entity.Transfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
}
