package br.com.itau.ia.service.money.transfer.application.message.queue;

import br.com.itau.ia.service.money.transfer.domain.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

@Component
public class TransferQueue {
    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private TransferService transferService;

    @JmsListener(destination = "${br.com.itau.ia.service.money.transfer.queue.money-transfer}", containerFactory = "rpcJmsFactory")
    public void receiveRequestAllQuestions(@Payload Map message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
        jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                int accountFromId = Integer.parseInt(message.get("accountFromId").toString());
                int accountFromTo = Integer.parseInt(message.get("accountFromTo").toString());
                BigDecimal value = new BigDecimal(message.get("value").toString());
                Message messageTosend = null;
                try {
                    messageTosend = session.createObjectMessage(
                            (Serializable) transferService.doTransfer(accountFromId, accountFromTo, value));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                messageTosend.setJMSCorrelationID(jmsMessageHeaderAccessor.getCorrelationId());
                return messageTosend;
            }
        });
    }
}
