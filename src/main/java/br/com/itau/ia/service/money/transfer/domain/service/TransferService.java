package br.com.itau.ia.service.money.transfer.domain.service;

import br.com.itau.ia.service.money.transfer.domain.entity.Transfer;
import br.com.itau.ia.service.money.transfer.domain.exception.WithoutBalance;
import br.com.itau.ia.service.money.transfer.domain.interfaces.balance.AccountBalanceService;
import br.com.itau.ia.service.money.transfer.domain.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;

@Service
public class TransferService {
    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private AccountBalanceService accountBalanceService;

    public Transfer doTransfer(int accountFromId, int accountToId, BigDecimal value) throws WithoutBalance {
        Transfer transfer = new Transfer(accountFromId, accountToId, value, Calendar.getInstance());
        /// Verificar se as contas existem e estão ativas. Bloqueios?
        if (this.accountBalanceService.hasBallance(accountFromId, value)) {
            this.transferRepository.save(transfer);
            return transfer;
        }
        throw new WithoutBalance("Saldo insuficiente!");
    }
}
