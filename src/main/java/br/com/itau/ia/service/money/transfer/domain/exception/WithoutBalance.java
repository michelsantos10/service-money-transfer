package br.com.itau.ia.service.money.transfer.domain.exception;

public class WithoutBalance extends Exception {
    public WithoutBalance(String message) {
        super(message);
    }

    public WithoutBalance(String message, Exception cause) {
        super(message, cause);
    }

}
