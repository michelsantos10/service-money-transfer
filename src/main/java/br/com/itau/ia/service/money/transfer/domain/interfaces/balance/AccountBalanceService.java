package br.com.itau.ia.service.money.transfer.domain.interfaces.balance;

import java.math.BigDecimal;

public interface AccountBalanceService {
    boolean hasBallance(int acoountId, BigDecimal value);
}
