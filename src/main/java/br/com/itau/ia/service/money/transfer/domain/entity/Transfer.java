package br.com.itau.ia.service.money.transfer.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

@Entity
@Table(name = "transfer")
public class Transfer {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true)
    private long id;

    @Column(name = "id_account_from")
    private int idAccountFrom;

    @Column(name = "id_account_to")
    private int idAccountTo;

    @Column(name = "value", precision = 15, scale = 9)
    private BigDecimal value;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Calendar date;

    public Transfer() {

    }

    public Transfer(int idAccountFrom, int idAccountTo, BigDecimal value, Calendar date) {
        this();
        this.idAccountFrom = idAccountFrom;
        this.idAccountTo = idAccountTo;
        this.value = value;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIdAccountFrom() {
        return idAccountFrom;
    }

    public void setIdAccountFrom(int idAccountFrom) {
        this.idAccountFrom = idAccountFrom;
    }

    public int getIdAccountTo() {
        return idAccountTo;
    }

    public void setIdAccountTo(int idAccountTo) {
        this.idAccountTo = idAccountTo;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
