
drop table if exists transfer;

CREATE TABLE transfer(
id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
id_account_from INT not null,
id_account_to INT not null,
value DECIMAL(15, 9) not null,
date DATETIME not null
);


